/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.linkstack;

/**
 *
 * @author informatics
 */
public class LinkStackApp {

    public static void main(String[] args) {
        LinkStack theStack = new LinkStack();
        theStack.push(20); // push items
        theStack.push(40);
        theStack.displayStack(); // display stack
        theStack.push(60); // push items
        theStack.push(80);
        theStack.displayStack(); // display stack
        theStack.pop(); // pop items
        theStack.pop();
        theStack.displayStack();
        
    }
}
